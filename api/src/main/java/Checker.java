import java.util.List;
import java.util.Map;

public interface Checker {
    boolean collectionCheck(Person person, ListOfPeople persons);

    List<Person> showPersons(ListOfPeople persons);

    Map<Person, Integer> repeats(ListOfPeople persons);
}
