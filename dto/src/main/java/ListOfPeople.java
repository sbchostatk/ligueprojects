import java.util.ArrayList;
import java.util.List;

public class ListOfPeople {
    List<Person> personList;

    public ListOfPeople() {
        personList = new ArrayList<Person>();
        personList.add(new Person("Андрей", "Акимов", "Иванович", 15, "мужчина", "27.11.1984"));
        personList.add(new Person("Татьяна", "Лосева", "Анатольевна", 24, "женщина", "11.12.1986"));
        personList.add(new Person("Иван", "Коньков", "Иванович", 35, "мужчина", "25.11.1974"));
        personList.add(new Person("Иван", "Коньков", "Иванович", 35, "мужчина", "25.11.1974"));
    }

    public List<Person> getPersonList() {
        return personList;
    }
}
