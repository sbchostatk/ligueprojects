
public class Person {
    private String name;
    private String lastName;
    private String middleName;
    private int age;
    private String sex;
    private String birthday;

    Person(String name, String lastName, String middleName, int age, String sex, String birthday) {
        this.age = age;
        this.birthday = birthday;
        this.name = name;
        this.lastName = lastName;
        this.sex = sex;
        this.middleName = middleName;
    }

    String getName() {return name;}
    String getLastName() {return lastName;}
    String getMiddleName() {return middleName;}
    int getAge() {return age;}
    String getSex() {return sex;}
    String getBirthday() {return birthday;}

    @Override
    public boolean equals(Object o) {
        Person person = (Person) o;
        return name.equals(person.name) && lastName.equals(person.lastName) && middleName.equals(person.middleName) &&
                age == person.age && sex.equals(person.sex) && birthday.equals(person.birthday);
    }

    @Override
    public String toString() {
        return name + " " + middleName + " " + lastName + ", " + sex + ", " + age + ", " + birthday;
    }
}
