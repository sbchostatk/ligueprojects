insert into location values (1,'Юности','Зеленоград');
insert into location values (2,'Старости','Москва');
insert into location values (3,'Молодости','Пермь');
insert into location values (4,'Детсва','Самара');
insert into location values (5,'Строителей','Саратов');
insert into location values (6,'Учителей','Орёл');
insert into location values (7,'Родителей','Торжок');
insert into location values (8,'Родителей','Тверь');
insert into location values (9,'Родителей','Белгород');
insert into location values (10,'Водителей','Курск');

insert into jobs values
('1', 'Охранник', 15000, 20000),
('2', 'Программист', 50000, 90000),
('3', 'Дизайнер', 40000, 80000),
('4', 'HR-директор', 30000, 90000),
('5', 'Стажер', 0, 20000),
('6', 'Архитектор', 45000, 70000),
('7', 'Тестировщик', 30000, 80000),
('8', 'Аналитик', 30000, 80000),
('9', 'PR-менеджер', 50000, 70000),
('10', 'Куратор проекта', 40000, 90000);

insert into departments (department_id, department_name, location_id) values
(1, 'Отдел продаж', 1),
(2, 'Отдел разработки', 2),
(3, 'Отдел дизайна', 3),
(4, 'Отдел кадров', 4),
(5, 'Охрана', 5),
(6, 'Руководстводящий отдел', 6),
(7, 'Отдел тестирования', 7),
(8, 'Отдел аналитики', 8);

insert into job_history values
(1, '2000-9-11', '2001-9-11', 1),
(2, '2001-11-9', '2002-9-11', 2),
(3, '2002-9-11', '2003-9-11', 3),
(4, '2003-9-11', '2004-9-11', 4),
(5, '2004-9-11', '2005-9-11', 5),
(6, '2005-9-11', '2006-9-11', 6),
(7, '2006-9-11', '2007-9-11', 7),
(8, '2008-9-11', '2008-9-11', 8);

insert into employees values (1,'Иван','Иванов','ivan@mail.ru','8800','2020-10-11','1',10000,1,1);
insert into employees values (2,'Петр','Петр','petr@mail.ru','8793','2019-08-15','1',1000,2,3);
insert into employees values (3,'Катя','Петрова','kate@mail.ru','8962','2017-05-19','2',80000,3,3);
insert into employees values (4,'Вася','Васильев','vasya@mail.ru','8953','2003-07-14','3',11000,4,1);
insert into employees values (5,'Дима','Дмитриев','dima@mail.ru','8977','2005-01-23','4',15000,5,4);

update departments set manager_id = 1 where department_id = 1;
update departments set manager_id = 2 where department_id = 2;
update departments set manager_id = 3 where department_id = 3;
update departments set manager_id = 4 where department_id = 4;
update departments set manager_id = 5 where department_id = 5;
update departments set manager_id = 1 where department_id = 6;
update departments set manager_id = 2 where department_id = 7;
update departments set manager_id = 3 where department_id = 8;

update job_history set department_id = 1 where employee_id = 1;
update job_history set department_id = 2 where employee_id = 2;
update job_history set department_id = 3 where employee_id = 3;
update job_history set department_id = 4 where employee_id = 4;
update job_history set department_id = 5 where employee_id = 5;

insert into jobs values
('11', 'Охранник', 15000, 20000),
('12', 'Программист', 50000, 90000),
('13', 'Дизайнер', 40000, 80000),
('14', 'Охранник', 15000, 20000),
('15', 'Программист', 50000, 90000),
('16', 'Дизайнер', 40000, 80000);

insert into employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id) values (6,'Даниил','Иванов','daniil@mail.ru','8801','2017-10-11','7',40000,6);
insert into employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id) values (7,'Надежда','Жукова','nadejda@mail.ru','8802','2016-10-11','6',20000, 7);



