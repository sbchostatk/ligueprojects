
--///////////////////////// 1 задание /////////////////////////////

select count(*) as repetitions, job_title, min_salary, max_salary from jobs
group by job_title, min_salary, max_salary
having count(*) > 1;

--///////////////////////// 2 задание /////////////////////////////

select max(e.salary) as max_salary, d.department_name
from employees as e, departments as d
where e.department_id = d.department_id
group by e.department_id, d.department_name;

--///////////////////////// 3 задание /////////////////////////////

select * from jobs as j
join employees e on j.job_id = e.job_id;

--///////////////////////// 4 задание /////////////////////////////

delete from jobs j1
where j1.ctid <>
      (select min(j2.ctid)
       from   jobs j2
       where  j1.job_title = j2.job_title
         and j1.min_salary = j2.min_salary
         and j1.max_salary = j2.max_salary);

--///////////////////////// 5 задание /////////////////////////////

select * from departments as d1
where (select count(*) from departments as d2
       where d1.department_id >= d2.department_id) <= (select count(*) from departments) / 2;

--///////////////////////// 6 задание /////////////////////////////

select * from employees where department_id is null;

--///////////////////////// 7 задание /////////////////////////////

CREATE VIEW EmployeeDepartmentLocation AS
SELECT e.first_name,
       e.last_name,
       d.department_name,
       l.city
FROM employees e INNER JOIN departments d on e.department_id = d.department_id
                 INNER JOIN location l on d.location_id = l.location_id;

--///////////////////////// 8 задание /////////////////////////////

create procedure showJobHistory()
    language sql
as $$
select * from job_history;
$$;

create or replace function show_job_history()
    returns table (employee_id int, start_date date, end_date date, job_id varchar, department_id int) as $$
begin
    return query
        select j.employee_id, j.start_date, j.end_date, j.job_id, j.department_id
        from job_history as j;
end;
$$ language plpgsql;

--///////////////////////// 9 задание /////////////////////////////

create procedure insertJob(id int, name varchar, min int, max int)
    language sql
as $$
insert into jobs values (id, name, min, max);
$$;

--///////////////////////// 10 задание /////////////////////////////

create function check_job() returns trigger as $check_job$
begin
    if new.job_title is null then
        raise exception 'job_title cannot be null';
    end if;

    if new.min_salary is null or new.max_salary is null then
        raise exception 'job cannot have null min or max salary';
    end if;

    if new.min_salary < 0 then
        raise exception 'salary cannot be negative';
    end if;
    return new;
end
$check_job$ language plpgsql;

create trigger check_job before insert on jobs
    for each row execute procedure check_job();

--///////////////////////// 11 задание /////////////////////////////

select d.department_name from departments as d, employees as e
where e.department_id = d.department_id
  and e.salary > 5000 and e.salary < 50000
group by d.department_name;

