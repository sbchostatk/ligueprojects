import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonTreatmentTest {

    PersonTreatment pt = new PersonTreatment();
    ListOfPeople listOfPeople = new ListOfPeople();

    @Test
    public void collectionCheck() {
        Person x = new Person("Иван", "Коньков", "Иванович", 35, "мужчина", "25.11.1974");
        Assert.assertTrue(pt.collectionCheck(x, listOfPeople));
    }

    @Test
    public void showPersons() {
        List<Person> expected = new ArrayList<>();
        expected.add(new Person("Андрей", "Акимов", "Иванович", 15, "мужчина", "27.11.1984"));

        List<Person> actual = pt.showPersons(listOfPeople);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void repeats() {
        Map<Person, Integer> expected = new HashMap<>();

        expected.put(new Person("Андрей", "Акимов", "Иванович", 15, "мужчина", "27.11.1984"), 1);
        expected.put(new Person("Татьяна", "Лосева", "Анатольевна", 24, "женщина", "11.12.1986"), 1);
        expected.put(new Person("Иван", "Коньков", "Иванович", 35, "мужчина", "25.11.1974"), 2);

        Map<Person, Integer> actual = pt.repeats(listOfPeople);
        Assert.assertEquals(expected, actual);
    }
}