import java.util.*;
import java.util.regex.Pattern;

public class PersonTreatment implements Checker {
    @Override
    public boolean collectionCheck(Person person, ListOfPeople persons) {
        for (int i = 0; i < persons.getPersonList().size(); i++) {
            if (persons.getPersonList().get(i).equals(person))
                return true;
        }
        return false;
    }

    @Override
    public List<Person> showPersons(ListOfPeople persons) {
        List p = new ArrayList<Person>();
        for (int i = 0; i < persons.getPersonList().size(); i++) {
            if (persons.getPersonList().get(i).getAge() <= 20 && Pattern.matches("\\b[А-Да-д][а-яё\\-]*", persons.getPersonList().get(i).getLastName()))
                p.add(persons.getPersonList().get(i));
        }
        return p;
    }

    @Override
    public Map<Person, Integer> repeats(ListOfPeople persons) {
        List <Person> list = persons.getPersonList();
        //Collections.sort(list, Comparator.comparingInt(Person::getAge).reversed());
        Map<Person, Integer> map = new HashMap<>();
        int flag = 0;
        for (Person p : list) {
            for (Map.Entry<Person, Integer> entry : map.entrySet()) {
                if (p.equals(entry.getKey())) {
                    entry.setValue(entry.getValue() + 1);
                    flag = 1;
                    break;
                }
            }
            if(flag == 0)
                map.put(p, 1);
            else
                flag = 0;
        }
        return map;
    }


}
