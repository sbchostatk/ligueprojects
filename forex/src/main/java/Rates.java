import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Rates {
    private HashMap<String, Double> rates;
    JSONParse jsonParse;
    private String base;
    private String date;

    Rates(String url) throws IOException, ParseException {
        jsonParse = new JSONParse();
        JSONObject jo = jsonParse.parser(url);
        base = (String) jo.get("base");
        date = (String) jo.get("date");
        JSONObject jsonRates = (JSONObject) jo.get("rates");
        rates = new Gson().fromJson(jsonRates.toString(), HashMap.class);
    }

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public HashMap<String, Double> getRates() {
        return rates;
    }

    @Override
    public String toString() {
        String result = "Курс валют по отношению к " + base + " на " + date + ":\n";
        for (Map.Entry<String, Double> entry : rates.entrySet()) {
            result += entry.getKey() + ": " + entry.getValue() + "\n";
        }
        return result;
    }
}
