import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Scanner;

public class ForexService {
    public static void main(String[] args) throws ParseException {
        Scanner in = new Scanner(System.in);

        System.out.println("Этот сервис предназначен для вывода курсов валют. Введите валюту, " +
                "по отношению к которой будет выведен курс");
        boolean success = false;

        while (!success) {
            try {
                String currency = in.nextLine();
                Rates rates = new Rates("https://api.exchangeratesapi.io/latest?base=" + currency.toUpperCase());
                System.out.println(rates.toString());
                success = true;
            } catch (IOException e) {
                System.out.println("Неверный ввод данных. Повторите попытку");
            }
        }
    }
}
